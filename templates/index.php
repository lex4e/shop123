<h1>Products</h1>
<div id="product-list">
	<ul>
		<?php foreach ($products as $product) : ?>
		<li>
			<h2><a href="/product.php?id=<?=$product['id']?>"><?=$product['title']?></a></h2>
			<div class="picture">
				<a href="/product.php?id=<?=$product['id']?>"><img src="imgs/<?=$product['picture']?>"></a>								
			</div>
			<div class="price">$ <span><?=$product['price']?></span></div>
			<div class="buy-button-container">
				<a href="/add2basket.php?id=<?=$product['id']?>" class="btn btn-buy">BUY</a>
			</div>
		</li>
		<?php endforeach; ?>						
	</ul>
	<div id="pager">
		<?=$paginationAsHtml ?? ''?>
	</div>
</div>
