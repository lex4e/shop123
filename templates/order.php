<h1>Order # <?=$id?></h1>
<div id="my-order">
	<table cellspacing="0"  border="1">
		<tr>
			<th>#</th>
			<th>Picture</th>
			<th>Sku</th>
			<th>Title</th>
			<th>Price</th>
		</tr>
		<?php 
		$total = 0.0;
		foreach ($products as $product) : 
			$total += floatval($product['price']);
		?>
		<tr>
			<td><?=$product['id']?></td>
			<td width="100px"><a href="/product.php?id=<?=$product['id']?>" target="_blank">
				<img src="imgs/<?=$product['picture']?>" width="100%"></a></td>								
			<td><?=$product['sku']?></td>
			<td><?=$product['title']?></td>
			<td><?=$product['price']?></td>
		</tr>
		<?php endforeach; ?>						
		<tr>
			<td colspan="4"><div style="text-align: right;">Total:</div></td>
			<td><?=$total?></td>
		</tr>
	</table>
</div>