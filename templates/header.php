<!DOCTYPE>
<html>
	<head>
		<title>Shop 123</title>
		<link rel="stylesheet" href="/css/styles.css">
	</head>
	<body>
		<div id="page-wrapper">
			<div id="header">
				<div id="logo">
					<a href="/"><img src="/imgs/logo.png"></a>
				</div>
				<div id="top-menu">
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="">Shoes</a></li>
						<li><a href="">Jeans</a></li>
						<li><a href="">Sweater</a></li>
						<li><a href="">Jacket</a></li>
					</ul>
				</div>
				<div id="basket-link">
					<a href="/basket.php"><img src="/imgs/basket.png"></a>
				</div>
			</div>
			<div id="content">
