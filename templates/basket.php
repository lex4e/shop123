<h1>My Order</h1>
<div id="my-order">
	<?php if (empty($products)) : ?>
		<div>Basket is Empty</div>
	<?php else : ?>
		<table cellspacing="0"  border="1">
			<tr>
				<th>#</th>
				<th>Picture</th>
				<th>Sku</th>
				<th>Title</th>
				<th>Price</th>
				<th></th>
			</tr>
			<?php 
			$total = 0.0;
			foreach ($products as $product) : 
				$total += floatval($product['price']);
			?>
			<tr>
				<td><?=$product['id']?></td>
				<td width="100px"><a href="/product.php?id=<?=$product['id']?>" target="_blank">
					<img src="imgs/<?=$product['picture']?>" width="100%"></a></td>								
				<td><?=$product['sku']?></td>
				<td><?=$product['title']?></td>
				<td><?=$product['price']?></td>
				<td width="100px" class="buy-button-container">
					<a href="/delete_from_basket.php?id=<?=$product['id']?>" class="btn btn-delete">delete</a>
				</td>
			</tr>
			<?php endforeach; ?>						
			<tr>
				<td colspan="5"><div style="text-align: right;">Total:</div></td>
				<td><?=$total?></td>
			</tr>
		</table>
		<form action="/create_order.php" method="post" class="form">
			<div><label>Name:</label><input type="text" name="order[name]" required></div>
			<div><label>Email:</label><input type="email" name="order[email]" required></div>
			<div><label>Phone:</label><input type="text" name="order[phone]" required></div>
			<div><label>Address:</label><input type="text" name="order[address]"></div>
			<div><label>Comment:</label><textarea name="order[comment]"></textarea></div>
			<div><input type="submit" value="Order" class="btn btn-buy"></div>
		</form>	
	<?php endif; ?>
</div>
