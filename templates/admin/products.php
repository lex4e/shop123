<h1>Products</h1>
<div style="padding: 10px 0; height: 60px;">
	<a href="/admin/product_create.php" class="btn btn-add">add</a>
</div>
<div id="my-order">
	<table cellspacing="0"  border="1">
		<tr>
			<th>#</th>
			<th>Picture</th>
			<th>Title</th>
			<th>Sku</th>
			<th>Price</th>
			<th>Category</th>
			<th></th>
		</tr>
		<?php foreach ($products as $product) : ?>
		<tr>
			<td><?=$product['id']?></td>
			<td><img src="/imgs/<?=$product['picture']?>" width="100px"></td>
			<td><a href="/product.php?id=<?=$product['id']?>" target="_blank"><?=$product['title']?></a></td>
			<td><?=$product['sku']?></td>
			<td><?=$product['price']?></td>
			<td><?=$product['category']?></td>
			<td width="100px" class="buy-button-container">
				<a href="/admin/product_edit.php?id=<?=$product['id']?>" 
					class="btn btn-buy">edit</a>
				<br>
				<a href="/admin/product_delete.php?id=<?=$product['id']?>" class="btn btn-delete">delete</a>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div id="pager"><?=$paginationAsHtml ?? ''?></div>
</div>
