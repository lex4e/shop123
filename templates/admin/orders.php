<?php 
include_once __DIR__ . "/../../models/Order.php";
?>

<h1>Orders</h1>
<div id="my-order">
	<table cellspacing="0"  border="1">
		<tr>
			<th>#</th>
			<th>Date</th>
			<th>Status</th>
			<th>Total</th>
			<th></th>
		</tr>
		<?php foreach ($orders as $order) : ?>
		<tr>
			<td><a href="/order.php?id=<?=$order['id']?>" target="_blank"><?=$order['id']?></a></td>
			<td><?=$order['created']?></td>
			<td><?=Order::getStatuses()[$order['status']]?></td>
			<td><?=$order['total']?></td>
			<td width="100px" class="buy-button-container">
				<?php if (!in_array($order['status'], [20, 30])) : ?>
					<a href="/admin/change_order_status.php?id=<?=$order['id']?>&status=<?=$order['status']?>" 
						class="btn btn-buy">change</a>
					<br>
					<a href="/admin/cancel__order.php?id=<?=$order['id']?>" class="btn btn-delete">cancel</a>
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div id="pager"><?=$paginationAsHtml ?? ''?></div>
</div>
