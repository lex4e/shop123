<h1>New Product</h1>
<form action="/admin/product_save.php" method="post" class="form" enctype="multipart/form-data">
	<input type="hidden" name="product[id]" value="<?=$product['id'] ?? ''?>">
	<div>
		<label>Title:</label>
		<input type="text" name="product[title]" value="<?=$product['title'] ?? ''?>">
	</div>
	<div>
		<label>Sku:</label>
		<input type="text" name="product[sku]" value="<?=$product['sku'] ?? ''?>">
	</div>
	<div>
		<label>Price:</label>
		<input type="text" name="product[price]" value="<?=$product['price'] ?? ''?>">
	</div>
	<div>
		<?php if (!empty($product['picture'] ?? null)) : ?>
			<div><img src="/imgs/<?=$product['picture']?>" width="100px"></div>
		<?php endif; ?>
		<label>Picture:</label>
		<input type="file" name="product[picture]">
	</div>
	<div>
		<label>Category:</label>
		<select name="product[category]">
			<option selected disabled></option>
			<option <?=($product['category'] ?? '') == 'shoes' ? 'selected checked' : ''?>>shoes</option>
			<option <?=($product['category'] ?? '') == 'jeans' ? 'selected checked' : ''?>>jeans</option>
			<option <?=($product['category'] ?? '') == 'sweater' ? 'selected checked' : ''?>>sweater</option>
			<option <?=($product['category'] ?? '') == 'jacket' ? 'selected checked' : ''?>>jacket</option>
		</select>
	</div>
	<div>
		<label>Content:</label>
		<textarea name="product[content]"><?=$product['content'] ?? ''?></textarea>
	</div>
	<div>
		<input type="submit" value="Create" class="btn btn-add">
	</div>
</form>