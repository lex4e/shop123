<div class="columns" id="product-wrapper">
	<div class="column">
		<div>
			<img src="imgs/<?=$product['picture']?>" class="product-preview">
		</div>
	</div>
	<div class="column second">						
		<h1><?=$product['title']?></h1>
		<div class="sku">Sku: <span><?=$product['sku']?></span></div>
		<div class="category">Category: <span><?=$product['category']?></span></div>
		<div class="description"><?=$product['content']?></div>
		<div class="price"><span>$</span> <?=$product['price']?></div>
		<div class="button-buy">
			<form action="#" method="POST">
				<input type="hidden" name="product[id]" value="<?=$product['id']?>">
				<input type="submit" class="btn btn-buy" value="BUY">
			</form>
		</div>
	</div>
</div>
