<?php 

include_once __DIR__ . "/Model.php";

class Product extends Model {
	
	public function getAll($page = 0)
	{
		$page = $page < 0 ? 0 : $page;
		$limit = self::NUMBER_PER_PAGE;
		$offset = $page * self::NUMBER_PER_PAGE;
		
		return $this->conn->query("select * from product limit $offset, $limit");
	}
	
	public function getById($id)
	{
		if ($id == 0) die('Bad Request');
		
		return $this->conn->query("select * from product where id=$id")
			->fetch_assoc();
	}
	
	public function save($id, $title, $sku, $price, $content, $picture, $category) {
		if ($id > 0) {
			$pictureUpdateQuery = (!empty($picture)) ? " , picture = '$picture' " : '';
			
			$this->conn->query("update product 
				set 
				title = '$title', 
				sku = '$sku', 
				content = '$content', 
				price = $price, 
				category = '$category'
				$pictureUpdateQuery
				where id = $id limit 1
			");
				
		} else {
			$this->conn->query("insert into product (title, sku, content, price, picture, category) 
				values ('$title', '$sku', '$content', $price, '$picture', '$category');");
				
			$result = $this->conn->query("select last_insert_id() as product_id")->fetch_assoc();	
		}
		
			
		return $result['product_id'] ?? $id;
	}
	
	public function delete($id) {
		// TODO delete picture
		$this->conn->query("delete from product where id=$id limit 1");
	}
}
