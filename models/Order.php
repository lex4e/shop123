<?php 

session_start();

include_once __DIR__ . "/Model.php";

class Order extends Model {

	public static function getStatuses()
	{
		return [
			0 => 'New',
			10 => 'In process',
			20 => 'Completed',
			30 => 'Canceled',
		];
	}
	
	public function getBasketsProducts()
	{
		$ids = $_SESSION['shop123']['products'] ?? [];

		if (empty($ids)) {
			$products = [];
		} else {
			$conditions = "WHERE id IN " . sprintf("(%s)", implode(',', $ids));
			$products = $this->conn->query("select * from product $conditions") ?? [];	
		}
			
		return $products;
	}
	
	public function getOrdersProducts($id)
	{
		if (empty($id)) die('Bad request');

		$orderItems = $this->conn->query("select * from order_item where order_id = $id") ?? [];	
		
		$ids = [];
		foreach ($orderItems as $orderItem) {		
			$ids[] = $orderItem['product_id'];
		}
		
		if (empty($ids)) {
			die('Empty Order');
		} else {
			$conditions = "WHERE id IN " . sprintf("(%s)", implode(',', $ids));
			$products = $this->conn->query("select * from product $conditions") ?? [];	
		}
		
		return $products;
	}
	
	public function add2basket($productId)
	{
		if ($productId == 0) die("Bad Request");

		$_SESSION['shop123']['products'][] = $productId;
	}
	
	public function createOrder()
	{
		if (isset($_POST['order'])) {
	
			$ids = $_SESSION['shop123']['products'] ?? [];
			
			if (!empty($ids)) {
				$conditions = "WHERE id IN " . sprintf("(%s)", implode(',', $ids));
				$products = $this->conn->query("select * from product $conditions") ?? [];	

				$total = 0.0;
				foreach ($products as $product) {
					$total += floatval($product['price']); 	
				}
			
				$name = htmlspecialchars($_POST['order']['name']);
				$email = htmlspecialchars($_POST['order']['email']);
				$phone = htmlspecialchars($_POST['order']['phone']);
				$address = htmlspecialchars($_POST['order']['address']);
				$comment = htmlspecialchars($_POST['order']['comment']);
				$time = date("Y-m-d H:i:s", time());
				
				$this->conn->query("insert into `orders` (`name`, `email`, `phone`, `address`, `comment`, `total`, `created`)
					values ('$name', '$email', '$phone', '$address', '$comment', $total, '$time')");		
					
				$result = $this->conn->query("select last_insert_id() as order_id")->fetch_assoc();	
				$orderId = $result['order_id'];
				
				foreach ($products as $product) {
					$productId = $product['id'];
					$this->conn->query("insert into `order_item` (`product_id`, `order_id`)
						values ('$productId', '$orderId')");		
				}
				
				unset($_SESSION['shop123']['products']);
				
				header("Location: /order.php?id=" . $orderId);
				die();
			}
		}
	}
	
	public function deleteProductFromBasket($id)
	{
		if ($id == 0) die("Bad Request");

		foreach ($_SESSION['shop123']['products'] as $index => $productId) {
			if ($productId == $id) {
				unset($_SESSION['shop123']['products'][$index]);
			}
		}
	}
	
	public function getAll($page = 0)
	{
		$page = $page < 0 ? 0 : $page;
		$limit = self::NUMBER_PER_PAGE;
		$offset = $page * self::NUMBER_PER_PAGE;
		
		return $this->conn->query("select * from orders order by id desc limit $offset, $limit");
	}
	
	public function changeStatus($orderId, $status = null)
	{				
		$newStatus = null;
		if ($status != 30) {
			switch ($status) {
				case '0':  
					$newStatus = 10;
					break;
				case '10': 
					$newStatus = 20;
					break;
				default:
					$newStatus = $status;	
			}
		} else {
			$newStatus = $status;
		}
		
		$this->conn->query("update orders set status = '$newStatus' where id = $orderId limit 1");
	}
}
