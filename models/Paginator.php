<?php 

include_once __DIR__ . "/Model.php";
include_once __DIR__ . "/Product.php";
include_once __DIR__ . "/Order.php";

class Paginator extends Model {
	
	private $model;
	
	public function __construct(Model $model)
	{
		$this->model = $model;
		
		parent::__construct();
	}
	
	public function getTotalNumberByModel($modelName)
	{
		$result = $this->conn->query("select count(*) as total_number from " . strtolower($modelName))
			->fetch_assoc();
		
		return $result['total_number'];
	}
	
	
	public function getAll($page = 0)
	{
		return $this->model->getAll($page);
	}
	
	public function getHtmlView($modelName, $url)
	{
		$content = '';
		for ($page = 0, $i = 0; $page <= $this->getTotalNumberByModel($modelName); $i++, $page += self::NUMBER_PER_PAGE) {
			$content .= "<a href='$url?page=$i'>" . ($i + 1) . "</a>";
		}
		
		return $content;
	}
}
