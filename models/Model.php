<?php 

abstract class Model {

	const NUMBER_PER_PAGE = 9;
		
	protected $conn;
	
	public function __construct()
	{
		$this->conn = new mysqli("localhost", "root", "pass123", "shop123");
	}

	abstract public function getAll($page = 0);

}