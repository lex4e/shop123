<?php 
$id = intval($_GET['id'] ?? 0);

include_once __DIR__ . "/models/Order.php";	

$products = (new Order())->getOrdersProducts($id);

include_once __DIR__ . "/templates/header.php";

include_once __DIR__ . "/templates/order.php";

include_once __DIR__ . "/templates/footer.php";
