<?php 
include_once __DIR__ . "/models/Product.php";	
include_once __DIR__ . "/models/Paginator.php";	

$page = intval($_GET['page'] ?? 0);
$model = new Product();
$paginator = new Paginator($model);
$products = $paginator->getAll($page);
$paginationAsHtml = $paginator->getHtmlView('product', '/index.php');

include_once __DIR__ . "/templates/header.php";

include_once __DIR__ . "/templates/index.php";

include_once __DIR__ . "/templates/footer.php";