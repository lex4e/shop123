<?php 
include_once __DIR__ . "/models/Product.php";	

$id = intval($_GET['id'] ?? 0);

$product = (new Product())->getById($id);

include_once __DIR__ . "/templates/header.php";	

include_once __DIR__ . "/templates/product.php";	

include_once __DIR__ . "/templates/footer.php";
?>