<?php
include_once __DIR__ . "/models/Order.php";	

$id = intval($_GET['id'] ?? 0);

$products = (new Order())->add2basket($id);

header("Location: " . $_SERVER['HTTP_REFERER']);
