<?php 
include_once __DIR__ . "/models/Order.php";	

$products = (new Order())->getBasketsProducts();

include_once __DIR__ . "/templates/header.php";

include_once __DIR__ . "/templates/basket.php";

include_once __DIR__ . "/templates/footer.php";
?>
