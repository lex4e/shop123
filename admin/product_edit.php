<?php

include_once __DIR__ . "/../models/Product.php";

$id = intval($_GET['id'] ?? 0);
if (empty($id)) die("Bad Request");

$product = (new Product())->getById($id);

include_once __DIR__ . "/../templates/header.php";

include_once __DIR__ . "/../templates/admin/product_form.php";

include_once __DIR__ . "/../templates/footer.php";
