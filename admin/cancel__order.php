<?php
 
include_once __DIR__ . "/../models/Order.php";	

$id = intval($_GET['id'] ?? 0);

(new Order())->changeStatus($id, 30);

header("Location: /admin/orders.php");