<?php 

include_once __DIR__ . "/../models/Product.php";

if (isset($_GET['id'])) {
	
	$id = intval($_GET['id']);
	
	if (empty($id)) die("Bad Request");
	
	(new Product())->delete($id);
	
	header("Location: /admin/products.php");
}
