<?php
 
include_once __DIR__ . "/../models/Order.php";	
include_once __DIR__ . "/../models/Paginator.php";	

$page = intval($_GET['page'] ?? 0);
$model = new Order();
$paginator = new Paginator($model);
$orders = $paginator->getAll($page);
$paginationAsHtml = $paginator->getHtmlView('orders', '/admin/orders.php');

include_once __DIR__ . "/../templates/header.php";

include_once __DIR__ . "/../templates/admin/orders.php";

include_once __DIR__ . "/../templates/footer.php";
?>
