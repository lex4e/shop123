<?php
 
include_once __DIR__ . "/../models/Order.php";	

$id = intval($_GET['id'] ?? 0);
$status = intval($_GET['status']);
if (empty($id)) die('Bad Request');

(new Order())->changeStatus($id, $status);

header("Location: /admin/orders.php");