<?php 

include_once __DIR__ . "/../models/Product.php";

if (isset($_POST['product'])) {
	
	$id = intval($_POST['product']['id']);
	$title = htmlspecialchars($_POST['product']['title']);
	$sku = htmlspecialchars($_POST['product']['sku']);
	$price = floatval($_POST['product']['price']);
	$content = htmlspecialchars($_POST['product']['content']);
	$category = htmlspecialchars($_POST['product']['category']);
	$picture = null;
	
	if (copy($_FILES['product']['tmp_name']['picture'], __DIR__ . "/../imgs/" . $_FILES['product']['name']['picture'])) {
		$picture = $_FILES['product']['name']['picture'];
	}
	
	$id = (new Product())->save($id, $title, $sku, $price, $content, $picture, $category);
	
	header("Location: /product.php?id=$id");
}
